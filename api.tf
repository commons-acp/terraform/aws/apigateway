############################################
# API GATEWAY - Sets up & configure api gw
############################################
data "template_file" "template" {
  template = file(var.swagger_file_path)
  vars = var.api_template_vars
}

############################################
# API GATEWAY - Sets up & configure api gw
############################################

resource "aws_api_gateway_rest_api" "api_gw" {
  depends_on =[data.template_file.template]
  name        = "${var.app_name}-api"
  description = "${var.app_name} : API GW"
  body = data.template_file.template.rendered
}

resource "aws_api_gateway_usage_plan" "apigw_usage_plan" {
  depends_on =[aws_api_gateway_rest_api.api_gw,aws_api_gateway_stage.stage]
  name         = "${var.app_name}-usage-plan"
  description  = "${var.app_name} : Usage Plan for API GW"
  product_code = "MYCODE"

  api_stages {
    api_id = aws_api_gateway_rest_api.api_gw.id
    stage  = aws_api_gateway_stage.stage.stage_name
  }

  quota_settings {
    limit  = var.quota
    offset = 0
    period = "DAY"
  }

  throttle_settings {
    burst_limit = 5
    rate_limit  = 100
  }
}

resource "aws_api_gateway_api_key" "mykey" {
  count = var.api_key_name == "" ? 0 : 1
  name = var.api_key_name
}

resource "aws_api_gateway_usage_plan_key" "main" {
  count = var.api_key_name == "" ? 0 : 1
  key_id        = aws_api_gateway_api_key.mykey[0].id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.apigw_usage_plan.id
}
