output "api_gw_invoke_url" {
  value = aws_api_gateway_stage.stage.invoke_url
}
output "api_gw_stage_log_group_name" {
  value = aws_cloudwatch_log_group.apigw_stage.name
}
output "token" {
  value = [for api_key in aws_api_gateway_api_key.mykey: api_key.value]
  sensitive = true
}