resource "aws_api_gateway_authorizer" "api_gateway_authorizer" {
  for_each = var.authorizer_provider_arns
  depends_on =[aws_api_gateway_rest_api.api_gw]
  name                   = "${each.value.name}-authorizer"
  rest_api_id            = aws_api_gateway_rest_api.api_gw.id
  identity_source = "method.request.header.Authorization"
  type = "COGNITO_USER_POOLS"
  provider_arns = [each.value.arn]
}