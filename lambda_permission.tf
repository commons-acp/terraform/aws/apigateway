resource "aws_lambda_permission" "apigw_lambda_permission" {
  for_each = var.lambdas
   depends_on =[aws_api_gateway_rest_api.api_gw]
  action = "lambda:InvokeFunction"
  function_name = each.value.lambda_api_function_name
  principal = "apigateway.amazonaws.com"
  qualifier = each.value.lambda_api_function_alias
  source_arn = "${aws_api_gateway_rest_api.api_gw.execution_arn}/*/*"
  lifecycle {
    create_before_destroy = true
  }
}
