

#project variable
variable "app_name" {
  description = "Application name"
}
variable "api_subdomain" {
  description = "The api subdomain"
}
variable "swagger_file_path" {
  description = "The api swagger path"
}

variable "api_template_vars" {
  description = "The api swagger vars"
}

variable "authorizer_provider_arns" {
  description = "The authorizer arn"
  type = map(object({
    arn = string
    name = string
  }))
  default = {}
}
variable "lambdas" {
  description = "List of lambdas that need to be integrated"
  type = map(object({
    lambda_api_function_name = string
    lambda_api_function_alias = string
    }))
  default = {}
}

variable "api_key_name" {
  description = "The name of the Api Key"
  default = ""
}

variable "quota" {
  description = "The maximum number of requests that can be made in a given time period / day"
  default = 20
}